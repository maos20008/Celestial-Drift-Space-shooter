# Celestial-Drift-Space-shooter
3D Space Shooter game using DirectX 11. Implemented Collision detection, Billboards, Height maps, and Shadow mapping throughout the space environment.

![project update](https://user-images.githubusercontent.com/18353476/27512891-2a14368e-5906-11e7-88ab-f82abb51393c.png)

[Oculus Rift](https://www.oculus.com/rift/) Support(Still in Progress Alpha) located in the Oculus Support Branch.
![project update 1](https://user-images.githubusercontent.com/18353476/27512890-2a136ccc-5906-11e7-91ab-266065bb7eea.png)
![oculus](https://user-images.githubusercontent.com/18353476/38968671-5f74dcc2-4341-11e8-9f4e-37a31a6a205d.png)


To build this game you will need to install the following software.

[DirectX Software Development Kit](https://www.microsoft.com/en-us/download/details.aspx?id=6812)

[Visual Studio Community](https://www.visualstudio.com/vs/community/)
